﻿using System;

namespace CoinJarSample.Util
{
    public static class Require
    {
        public static void Positive(decimal value, string paramName)
        {
            if (value <= 0)
            {
                throw new ArgumentOutOfRangeException(paramName, value, "Only positive numbers allowed.");
            }
        }
    }
}