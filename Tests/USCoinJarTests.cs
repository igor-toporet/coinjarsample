﻿using System;
using System.Linq;
using CoinJarSample.Domain;
using CoinJarSample.Domain.US;
using Xunit;

namespace CoinJarSample.Tests
{
    public class USCoinJarTests
    {
        private readonly ICoinJar<USCoin> _jar = new CoinJar<USCoin>(32.FluidOunces());

        [Fact]
        public void Can_add_all_US_coinage()
        {
            _jar.Add(new Penny());
            _jar.Add(new Nickel());
            _jar.Add(new Dime());
            _jar.Add(new QuarterDollar());
            _jar.Add(new HalfDollar());
            _jar.Add(new Dollar());

            Assert.True(_jar.FreeVolume < _jar.Volume);
        }

        [Fact]
        public void Just_out_of_curiocity_determine_most_efficient_coin_to_collect()
        {
            var coins = new USCoin[]
            {
                new Penny(),
                new Nickel(),
                new Dime(),
                new QuarterDollar(),
                new HalfDollar(),
                new Dollar()
            };

            var rating = coins.Select(c => new {Coin = c, AmountPerVolumeRatio = c.FaceValue/c.Volume})
                .OrderByDescending(c => c.AmountPerVolumeRatio).ToArray();

            Console.WriteLine();
            Console.WriteLine("==== Most efficient coin to collect ( Type: AmountPerVolumeRatio ) ====");
            Console.WriteLine();

            foreach (var entry in rating)
            {
                Console.WriteLine("{0}: {1}", entry.Coin.GetType().Name, entry.AmountPerVolumeRatio);
            }

            Console.WriteLine();
            Console.WriteLine("==== Now lets see how many coins of each type might fit into 32 fl. ou. jar ====");
            Console.WriteLine();

            foreach (var entry in rating)
            {
                var coin = entry.Coin;

                int numOfCoins = HowManyCoinsRequiredToFillThatJar(coin);

                Console.WriteLine("{0}: {1} * {2} = ${3}",
                    coin.GetType().Name, numOfCoins, coin.FaceValue, numOfCoins*coin.FaceValue);
            }
        }

        private static int HowManyCoinsRequiredToFillThatJar(USCoin coin)
        {
            var jar = new CoinJar<USCoin>(32.FluidOunces());

            int result = 0;
            Exception exception;

            do
            {
                result++;
                exception = Record.Exception(() => jar.Add(coin));
            } while (exception == null);

            return result - 1;
        }
    }
}