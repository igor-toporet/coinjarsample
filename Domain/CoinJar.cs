﻿using System;
using CoinJarSample.Util;

namespace CoinJarSample.Domain
{
    public class CoinJar<TCoin> : ICoinJar<TCoin> where TCoin : ICoin
    {
        public CoinJar(decimal volume)
        {
            Require.Positive(volume, "volume");

            FreeVolume = Volume = volume;
        }

        public decimal FreeVolume { get; private set; }

        public decimal TotalAmountCollected { get; private set; }

        public decimal Volume { get; private set; }

        public void Add(TCoin coin)
        {
            if (coin.Volume > FreeVolume)
            {
                string message = string.Format("Coin volume {0} exceeds free volume of {1}", coin.Volume, FreeVolume);
                throw new InvalidOperationException(message);
            }

            TotalAmountCollected += coin.FaceValue;
            FreeVolume -= coin.Volume;
        }

        public void Empty()
        {
            FreeVolume = Volume;
            TotalAmountCollected = 0;
        }
    }
}