﻿using System;
using CoinJarSample.Util;

namespace CoinJarSample.Domain
{
    public abstract class Coin : ICoin
    {
        protected Coin(decimal faceValue, decimal diameter, decimal thickness)
        {
            Require.Positive(faceValue, "faceValue");
            Require.Positive(diameter, "diameter");
            Require.Positive(thickness, "thickness");

            FaceValue = faceValue;
            Diameter = diameter;
            Thickness = thickness;
        }

       

        public decimal Volume
        {
            get
            {
                decimal radius = Diameter/2;
                return new decimal(Math.PI)*radius*radius*Thickness;
            }
        }

        public decimal FaceValue { get; private set; }
        public decimal Diameter { get; private set; }
        public decimal Thickness { get; private set; }
    }
}