﻿namespace CoinJarSample.Domain
{
    public interface ICoin : I3DShape
    {
        decimal FaceValue { get; }
        decimal Diameter { get; }
        decimal Thickness { get; }
    }
}