﻿namespace CoinJarSample.Domain
{
    public interface I3DShape
    {
        decimal Volume { get; }
    }
}