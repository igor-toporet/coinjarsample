﻿namespace CoinJarSample.Domain
{
    public interface ICoinJar<in TCoin> where TCoin : ICoin
    {
        decimal FreeVolume { get; }

        decimal TotalAmountCollected { get; }

        decimal Volume { get; }

        void Add(TCoin coin);

        void Empty();
    }
}