﻿namespace CoinJarSample.Domain.US
{
    public abstract class USCoin : Coin
    {
        protected USCoin(decimal faceValue, decimal diameter, decimal thickness) : base(faceValue, diameter, thickness)
        {
        }
    }

    //
    // http://www.usmint.gov/about_the_mint/?action=coin_specification
    //

    public class Penny : USCoin
    {
        public Penny() : base(1.Cents(), 0.750.Inches(), 1.52.Millimeters())
        {
        }
    }

    public class Nickel : USCoin
    {
        public Nickel() : base(5.Cents(), 0.835.Inches(), 1.95.Millimeters())
        {
        }
    }

    public class Dime : USCoin
    {
        public Dime() : base(0.1m, 0.705.Inches(), 1.35.Millimeters())
        {
        }
    }

    public class QuarterDollar : USCoin
    {
        public QuarterDollar() : base(25.Cents(), 0.955.Inches(), 1.75.Millimeters())
        {
        }
    }

    public class HalfDollar : USCoin
    {
        public HalfDollar() : base(50.Cents(), 1.205.Inches(), 2.15.Millimeters())
        {
            // http://www.usmint.gov/about_the_mint/?action=coin_specification
        }
    }

    public class Dollar : USCoin
    {
        public Dollar() : base(100.Cents(), 26.5.Millimeters(), 2.Millimeters())
        {
            // http://www.usmint.gov/about_the_mint/?action=coin_specifications
        }
    }
}