﻿namespace CoinJarSample.Domain.US
{
    public static class ConversionExtensions
    {
        /// <summary>
        ///     Converts fluid ounces amount to metric system
        /// </summary>
        /// <param name="fluidOunces">volume in fluid ounces</param>
        /// <returns>volume in cubic meters</returns>
        public static decimal FluidOunces(this decimal fluidOunces)
        {
            //
            // https://www.wolframalpha.com/input/?i=fluid+ounce
            //
            const decimal numerator = 473176473;
            const decimal denominator = 16000000000000;

            return fluidOunces*numerator/denominator;
        }

        /// <summary>
        ///     Converts inches to metric system
        /// </summary>
        /// <param name="inches">length in inches</param>
        /// <returns>length in meters</returns>
        public static decimal Inches(this decimal inches)
        {
            //
            // https://www.wolframalpha.com/input/?i=inch
            //
            const decimal factor = 0.0254m;
            return inches*factor;
        }

        public static decimal Inches(this double inches)
        {
            return Inches(new decimal(inches));
        }

        public static decimal Inches(this int inches)
        {
            return Inches(new decimal(inches));
        }

        public static decimal Cents(this int cents)
        {
            return decimal.Divide(cents,100);
        }

        /// <summary>
        ///     Converts millimeters to metric system
        /// </summary>
        /// <param name="millimeters">length in millimeters</param>
        /// <returns>length in meters</returns>
        public static decimal Millimeters(this decimal millimeters)
        {
            const int oneThousand = 1000;
            return millimeters/oneThousand;
        }

        public static decimal Millimeters(this double millimeters)
        {
            return Millimeters(new decimal(millimeters));
        }

        public static decimal Millimeters(this int millimeters)
        {
            return Millimeters(new decimal(millimeters));
        }

        public static decimal FluidOunces(this int fluidOunces)
        {
            return FluidOunces(new decimal(fluidOunces));
        }

        public static decimal FluidOunces(this double fluidOunces)
        {
            return FluidOunces(new decimal(fluidOunces));
        }
    }
}